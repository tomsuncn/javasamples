package me.sunjie.springbootsample.redis;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import redis.clients.jedis.JedisShardInfo;

@Aspect
@Component
public class ServiceMonitor {

	
	
	@Before("execution(* me.sunjie.springbootsample.redis.*Service.*(..))")
	public void changeRedisTemplate(JoinPoint joinPoint) {
//		System.out.println("Completed: " + joinPoint);
		System.out.println("Before: " + joinPoint);
		String bussinessMethodName=joinPoint.getSignature().getName();
		Object[] argArr=joinPoint.getArgs();
		Class[] argClsArr=new Class[argArr.length];
		for(int index=0;index<argArr.length;index++){
			argClsArr[index]=argArr[index].getClass();
		}
		Class serviceCls=joinPoint.getTarget().getClass();
		try {
			Method method=serviceCls.getMethod("setSpringRedisTemplate", StringRedisTemplate.class);
			Method bussinessMethod=serviceCls.getMethod(bussinessMethodName, argClsArr);
			
			TargetDataSource targetDataSource=bussinessMethod.getAnnotation(TargetDataSource.class);
			String redisHost=targetDataSource.host();
			int redisPort=targetDataSource.port();
			int databaseIndex=targetDataSource.index();
			JedisShardInfo shardInfo1=new JedisShardInfo(redisHost,redisPort);
			JedisConnectionFactory connectionFactory=new JedisConnectionFactory(shardInfo1);
			connectionFactory.setDatabase(databaseIndex);
			method.invoke(joinPoint.getTarget(), new StringRedisTemplate(connectionFactory));
		} catch (NoSuchMethodException | SecurityException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}

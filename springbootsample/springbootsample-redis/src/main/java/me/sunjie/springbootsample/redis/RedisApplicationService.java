package me.sunjie.springbootsample.redis;

import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

@Component
public class RedisApplicationService extends ServiceBase{
	
	@TargetDataSource(host = "localhost",port=6379,index=0)
	public void putValueToRedis(String key,String value){
//		JedisShardInfo shardInfo1=new JedisShardInfo("localhost",6379);
//		JedisConnectionFactory connectionFactory=new JedisConnectionFactory(shardInfo1);
//		springRedisTemplate=new StringRedisTemplate(connectionFactory);
		ValueOperations<String, String> ops = springRedisTemplate.opsForValue();
		ops.set(key, value);
		System.out.println("finish insert value to redis");
	}
}

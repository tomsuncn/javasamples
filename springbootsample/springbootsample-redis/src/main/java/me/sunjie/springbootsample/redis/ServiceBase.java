package me.sunjie.springbootsample.redis;

import org.springframework.data.redis.core.StringRedisTemplate;

public class ServiceBase {

	protected StringRedisTemplate springRedisTemplate;

	public void setSpringRedisTemplate(StringRedisTemplate springRedisTemplate) {
		this.springRedisTemplate = springRedisTemplate;
	}
	
}

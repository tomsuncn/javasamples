package me.sunjie.springbootsample.redis;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SampleRedisApplication implements CommandLineRunner {


	@Autowired
	private RedisApplicationService redisApplicationService;
	
	@Override
	public void run(String... args) throws Exception {
		redisApplicationService.putValueToRedis("test1","value1");
	}

	public static void main(String[] args) throws Exception {
		// Close the context so it doesn't stay awake listening for redis
		SpringApplication.run(SampleRedisApplication.class, args).close();
	}

}
